import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ILike, Repository } from 'typeorm';
import { LogsFilterDto } from './dto/logs.dto';
import { Log } from './entities/log.entity';

@Injectable()
export class LogsService {
  constructor(
    @InjectRepository(Log)
    private readonly logsRepo: Repository<Log>,
  ) {}

  async create(payload: Partial<Log>) {
    const result = await this.logsRepo.save(payload);
    return result;
  }

  async findByQuery(filter: LogsFilterDto) {
    const take = filter.limit || 10;
    const page = filter.page || 1;
    const skip = (page - 1) * take;
    const result = this.logsRepo.findAndCount({
      where: {
        message: filter?.search ? ILike(`%${filter.search}%`) : undefined,
      },
      skip,
      take,
      order: { [filter.sortBy]: filter.sortOrder },
    });
    return result;
  }

  async findById(id: number) {
    const result = await this.logsRepo.findOneBy({ id });
    return result;
  }

  async updateById(id: number, payload: Partial<Log>) {
    const result = await this.logsRepo.update(id, payload);
    return result;
  }

  async deleteById(id: number) {
    const result = await this.logsRepo.delete(id);
    return result.affected;
  }
}
