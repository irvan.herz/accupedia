import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import {
  BadRequestException,
  Controller,
  Get,
  NotFoundException,
  Param,
  Post,
  Query,
} from '@nestjs/common';
import { LogsFilterDto } from './dto/logs.dto';
import { LogsService } from './logs.service';

@Controller('logs')
export class LogsController {
  constructor(
    private readonly logsSvc: LogsService,
    private readonly amqpConnection: AmqpConnection,
  ) {}

  @Get()
  async findByQuery(@Query() filter: LogsFilterDto) {
    const [data, numItems] = await this.logsSvc.findByQuery(filter);
    const numPages = Math.ceil(numItems / filter.limit);
    const meta = {
      page: filter.page,
      limit: filter.limit,
      numItems: numItems,
      numPages,
    };
    return { data, meta };
  }

  @Get(':id')
  async findById(@Param('id') id: number) {
    const log = await this.logsSvc.findById(id);
    if (!log) throw new NotFoundException();
    return {
      data: log,
    };
  }

  @Post(':id/retry')
  async retryById(@Param('id') id: number) {
    const log = await this.logsSvc.findById(id);
    if (!log) throw new NotFoundException();
    const logMeta = JSON.parse(log.meta);
    switch (log.group) {
      case 'companion.tokopedia.webhooks.order-notification':
        this.amqpConnection.publish(
          'companion',
          'companion.tokopedia.webhooks.order-notification',
          {
            data: logMeta.data,
          },
        );
        break;
      case 'companion.tokopedia.webhooks.order-status':
        this.amqpConnection.publish(
          'companion',
          'companion.tokopedia.webhooks.order-status',
          {
            data: logMeta.data,
          },
        );
        break;
      case 'companion.accurate.webhooks.item-quantity':
        this.amqpConnection.publish(
          'companion',
          'companion.accurate.webhooks.item-quantity',
          {
            data: logMeta.data,
          },
        );
        break;
      default:
        throw new BadRequestException();
    }
    await this.logsSvc.deleteById(log.id);
    return;
  }
}
