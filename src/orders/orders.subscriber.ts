import { RabbitSubscribe } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';
import { Log } from 'src/logs/entities/log.entity';
import { LogsService } from 'src/logs/logs.service';
import { OrdersService } from './orders.service';

@Injectable()
export class OrdersSubscriber {
  constructor(
    private readonly logsSvc: LogsService,
    private readonly ordersSvc: OrdersService,
  ) {}

  @RabbitSubscribe({
    name: 'processCreateOrderFromTokopedia',
    exchange: 'companion',
    routingKey: 'companion.tokopedia.webhooks.order-notification',
  })
  public async processCreateOrderFromTokopedia(payload: any) {
    console.log('processCreateOrderFromTokopedia');
    const { data } = payload;
    try {
      await this.ordersSvc.createFromTokopediaOrder(data);
      return true;
    } catch (err) {
      console.log('processCreateOrderFromTokopedia', err);

      await this.saveLog({
        group: 'companion.tokopedia.webhooks.order-notification',
        message: err?.message || 'Unknown error',
        meta: {
          data,
        },
      });
    }
  }

  private async saveLog(payload: Partial<Log>) {
    try {
      await this.logsSvc.create(payload);
    } catch (err) {}
  }
}
