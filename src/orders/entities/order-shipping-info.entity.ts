import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

type OrderSourceInfoCompanionType =
  | 'common'
  | 'tokopedia'
  | 'shopee'
  | 'lazada'
  | 'blibli'
  | 'bukalapak';

const ORDER_SOURCE_INFO_COMPANION_ENUM = [
  'common',
  'tokopedia',
  'shopee',
  'lazada',
  'blibli',
  'bukalapak',
];

@Entity()
export class OrderSourceInfo {
  @PrimaryGeneratedColumn()
  id: number;
  @Column('int')
  orderId: number;
  @Column('enum', {
    enum: ORDER_SOURCE_INFO_COMPANION_ENUM,
    default: 'common',
  })
  companion: OrderSourceInfoCompanionType;
  @Column('json', { default: {} })
  meta: object;
  @CreateDateColumn()
  createdAt: Date;
  @UpdateDateColumn()
  updatedAt: Date;
}
