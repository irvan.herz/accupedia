import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class OrderDetail {
  @PrimaryGeneratedColumn()
  id: number;
  @Column('int')
  orderId: number;
  @Column('varchar', { length: 300 })
  sku: string;
  @Column('varchar', { length: 300 })
  name: string;
  @Column('text', { nullable: true })
  description: string;
  @Column('int', { default: 0 })
  quantity: number;
  @Column('decimal', { default: 0 })
  unitPrice: number;
  @Column('decimal', { default: 0 })
  discountAmount: number;
  @Column('json', { default: {} })
  meta: object;
  @CreateDateColumn()
  createdAt: Date;
  @UpdateDateColumn()
  updatedAt: Date;
}
