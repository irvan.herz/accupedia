import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

type OrderStatusType =
  | 'created'
  | 'paid'
  | 'canceled'
  | 'rejected'
  | 'accepted'
  | 'packing'
  | 'dropoff'
  | 'pickup'
  | 'delivery'
  | 'delivered'
  | 'completed';

const ORDER_STATUS_ENUM = [
  'created',
  'paid',
  'canceled',
  'rejected',
  'accepted',
  'packing',
  'dropoff',
  'pickup',
  'delivery',
  'delivered',
  'completed',
];

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number;
  @Column('int', { default: 0 })
  customerId: number;
  @Column('text', { nullable: true })
  note: string;
  @Column('enum', { enum: ORDER_STATUS_ENUM, default: 'created' })
  status: OrderStatusType;
  @Column('decimal', { default: 0 })
  itemPrice: number;
  @Column('decimal', { default: 0 })
  openAmount: number;
  @Column('json', { default: {} })
  meta: object;
  @CreateDateColumn()
  createdAt: Date;
  @UpdateDateColumn()
  updatedAt: Date;
}
