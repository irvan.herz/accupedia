import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LogsModule } from 'src/logs/logs.module';
import { SharedRabbitMQModule } from 'src/shared/shared-rabbitmq.module';
import { OrderDetail } from './entities/order-detail.entity';
import { OrderSourceInfo } from './entities/order-source-info.entity';
import { Order } from './entities/order.entity';
import { OrdersController } from './orders.controller';
import { OrdersService } from './orders.service';
import { OrdersSubscriber } from './orders.subscriber';

@Module({
  imports: [
    SharedRabbitMQModule,
    LogsModule,
    TypeOrmModule.forFeature([Order, OrderDetail, OrderSourceInfo]),
  ],
  controllers: [OrdersController],
  providers: [OrdersService, OrdersSubscriber],
})
export class OrdersModule {}
