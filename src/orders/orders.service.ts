import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DataSource, ILike, Repository } from 'typeorm';
import { OrderFilterDto } from './dto/orders.dto';
import { OrderDetail } from './entities/order-detail.entity';
import { OrderSourceInfo } from './entities/order-source-info.entity';
import { Order } from './entities/order.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private readonly ordersRepo: Repository<Order>,
    @InjectRepository(OrderDetail)
    private readonly orderDetailsRepo: Repository<OrderDetail>,
    @InjectRepository(OrderSourceInfo)
    private readonly orderSrcInfoRepo: Repository<OrderSourceInfo>,
    private readonly dataSource: DataSource,
  ) {}

  async createWithDto(payload: Partial<Order>) {
    const result = await this.ordersRepo.save(payload, {});
    return result;
  }

  async createFromTokopediaOrder(payload: any) {
    const queryRunner = this.dataSource.createQueryRunner();

    await queryRunner.connect();
    await queryRunner.startTransaction();
    try {
      const order = await queryRunner.manager
        .withRepository(this.ordersRepo)
        .save({
          customerId: 0,
          itemPrice: 0,
          openAmount: 0,
        });
      const sourceInfo = await queryRunner.manager
        .withRepository(this.orderSrcInfoRepo)
        .save({
          companion: 'tokopedia',
          orderId: order.id,
        });
      const detail = await queryRunner.manager
        .withRepository(this.orderDetailsRepo)
        .save({
          orderId: order.id,
          name: 'Barang',
          description: 'Barang',
          sku: 'SKU',
          quantity: 1,
          unitPrice: 1000,
        });
      await queryRunner.commitTransaction();
      return {
        ...order,
        sourceInfo,
        details: [detail],
      };
    } catch (err) {
      await queryRunner.rollbackTransaction();
      throw err;
    } finally {
      // you need to release a queryRunner which was manually instantiated
      await queryRunner.release();
    }
  }

  async create(payload: Partial<Order>) {
    const result = await this.ordersRepo.save(payload);
    return result;
  }

  async save(payload: Partial<Order>) {
    const result = await this.ordersRepo.save(payload);
    return result;
  }

  async findByQuery(filter: OrderFilterDto) {
    const take = filter.limit || 10;
    const page = filter.page || 1;
    const skip = (page - 1) * take;
    const result = this.ordersRepo.findAndCount({
      where: {
        note: filter?.search ? ILike(`%${filter.search}%`) : undefined,
      },
      skip,
      take,
      order: { [filter.sortBy]: filter.sortOrder },
    });
    return result;
  }

  async updateById(id: number, payload: Partial<Order>) {
    const result = await this.ordersRepo.update({ id }, payload);
    return result.affected;
  }

  async findById(id: number) {
    const result = await this.ordersRepo.findOneBy({ id });
    return result;
  }

  async deleteById(id: number) {
    const result = await this.ordersRepo.delete(id);
    return result.affected;
  }
}
