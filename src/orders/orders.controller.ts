import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import {
  CreateOrderDto,
  OrderFilterDto,
  UpdateOrderDto,
} from './dto/orders.dto';
import { OrdersService } from './orders.service';

@Controller('orders')
export class OrdersController {
  constructor(private readonly ordersService: OrdersService) {}

  @Post()
  async create(@Body() payload: CreateOrderDto) {
    await this.ordersService.create(payload as any);
    return;
  }

  @Get()
  async findByQuery(@Query() filter: OrderFilterDto) {
    const [data, numItems] = await this.ordersService.findByQuery(filter);
    const numPages = Math.ceil(numItems / filter.limit);
    const meta = {
      page: filter.page,
      limit: filter.limit,
      numItems,
      numPages,
    };
    return { data, meta };
  }

  @Get(':id')
  async findById(@Param('id') id: number) {
    const data = await this.ordersService.findById(+id);
    return { data };
  }

  @Patch(':id')
  async updateById(@Param('id') id: number, @Body() payload: UpdateOrderDto) {
    await this.ordersService.updateById(+id, payload as any);
    return;
  }

  @Delete(':id')
  async deleteById(@Param('id') id: number) {
    await this.ordersService.deleteById(+id);
    return;
  }
}
