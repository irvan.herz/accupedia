import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as crypto from 'crypto';
import * as dayjs from 'dayjs';
import { readFile } from 'fs/promises';
import * as path from 'path';
import { ILike, Repository } from 'typeorm';
import { AccurateService } from './accurate.service';
import { TokopediaFilterDto } from './dto/tokopedia.dto';
import { Tokopedia } from './entities/tokopedia.entity';
import { TokopediaApiService } from './tokopedia-api.service';

@Injectable()
export class TokopediaService {
  constructor(
    @InjectRepository(Tokopedia)
    private readonly tokopediaRepo: Repository<Tokopedia>,
    private readonly accurateSvc: AccurateService,
    private readonly tokopediaApiSvc: TokopediaApiService,
  ) {}

  async create(payload: Partial<Tokopedia>) {
    const result = await this.tokopediaRepo.save(payload);
    return result;
  }

  async findByQuery(filter: TokopediaFilterDto) {
    const take = filter.limit || 10;
    const page = filter.page || 1;
    const skip = (page - 1) * take;
    const result = this.tokopediaRepo.findAndCount({
      where: {
        email: filter?.search ? ILike(`%${filter.search}%`) : undefined,
      },
      skip,
      take,
      order: { [filter.sortBy]: filter.sortOrder },
    });
    return result;
  }

  async updateById(id: number, payload: Partial<Tokopedia>) {
    const result = await this.tokopediaRepo.update({ id }, payload);
    return result.affected;
  }

  async findById(id: number) {
    const result = await this.tokopediaRepo.findOneBy({ id });
    return result;
  }

  async findByAccurateId(accurateId: number) {
    const result = await this.tokopediaRepo.findOneBy({ accurateId });
    return result;
  }

  async findByShopId(shopId: number) {
    const result = await this.tokopediaRepo.findOneBy({ shopId });
    return result;
  }

  async deleteById(id: number) {
    const result = await this.tokopediaRepo.delete(id);
    return result.affected;
  }

  async findAccurateByShopId(shopId: number) {
    const tokopedia = await this.tokopediaRepo.findOneBy({ shopId });
    if (!tokopedia?.accurateId) return null;
    const accurate = await this.accurateSvc.findById(tokopedia.accurateId);
    return accurate;
  }

  async auditCredentials(companion: Tokopedia) {
    const expiredAt = dayjs(companion.accessTokenExpiredAt);
    const now = dayjs().add(60, 'seconds');
    if (!companion.accessToken || expiredAt.isBefore(now)) {
      const grant = await this.tokopediaApiSvc.grantAccessToken(companion);
      if (!grant?.access_token)
        throw new Error(`Refresh token failed for shop ID ${companion.shopId}`);
      companion.accessToken = grant.access_token;
      companion.accessTokenExpiredAt = dayjs()
        .add(grant.expires_in, 'seconds')
        .toDate();
      companion = await this.tokopediaRepo.save(companion);
    }

    return companion;
  }

  async decryptContent(secret: string, content: string) {
    const privateKeyPath = path.resolve('./private/tokopedia_private_key.pem');
    const privateKey = await readFile(privateKeyPath);
    const debunkedKey = crypto.privateDecrypt(
      {
        key: privateKey,
        padding: crypto.constants.RSA_PKCS1_OAEP_PADDING,
        oaepHash: 'sha256',
      },
      Buffer.from(secret, 'base64'),
    );

    const buffContent = Buffer.from(content, 'base64');
    const nonce = buffContent.slice(
      buffContent.length - 12,
      buffContent.length,
    );
    const cipher = buffContent.slice(0, buffContent.length - 12);
    const tag = cipher.slice(cipher.length - 16, cipher.length);
    const data = cipher.slice(0, cipher.length - 16);
    const decipher = crypto.createDecipheriv('aes-256-gcm', debunkedKey, nonce);
    decipher.setAuthTag(tag);
    const dec = Buffer.concat([decipher.update(data), decipher.final()]);
    return JSON.parse(dec.toString());
  }
}
