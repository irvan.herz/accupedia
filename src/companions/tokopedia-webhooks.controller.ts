import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import {
  BadRequestException,
  Body,
  Controller,
  Post,
  Req,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as crypto from 'crypto';
import { LogsService } from 'src/logs/logs.service';
import { TokopediaService } from './tokopedia.service';

@Controller('tokopedia/webhooks')
export class TokopediaWebhooksController {
  constructor(
    private readonly amqpConnection: AmqpConnection,
    private readonly configSvc: ConfigService,
    private readonly tokopediaSvc: TokopediaService,
    private readonly logsService: LogsService,
  ) {}

  @Post('order-notification')
  onOrderNotification(@Body() body, @Req() req) {
    try {
      if (!this.verify(body.shop_id, req))
        throw new BadRequestException('Could not verify this webhook');
      this.amqpConnection.publish(
        'companion',
        'companion.tokopedia.webhooks.order-notification',
        {
          data: body,
        },
      );
    } catch (err) {
      this.saveLogError(err, body, 'tokopedia.webhook.order-notification');
    }
    return 'OK';
  }

  @Post('order-status')
  onOrderStatus(@Body() body, @Req() req) {
    try {
      if (!this.verify(body.shop_id, req))
        throw new BadRequestException('Could not verify this webhook');
      this.amqpConnection.publish(
        'companion',
        'companion.tokopedia.webhooks.order-status',
        {
          data: body,
        },
      );
    } catch (err) {
      this.saveLogError(err, body, 'tokopedia.webhook.order-status');
    }
    return 'OK';
  }

  @Post('order-cancellation')
  onOrderCancellation() {
    return 'OK';
  }

  private async verify(shopId, req) {
    try {
      const shouldVerify = this.configSvc.get<string>(
        'TOKOPEDIA_WEBHOOK_SHOULDVERIFY',
      );
      if (shouldVerify === '0') return true;
      const companion = await this.tokopediaSvc.findByShopId(shopId);
      if (!companion) return false;
      const apiKey = companion.clientId;
      const hmac = crypto
        .createHmac('sha256', apiKey)
        .update(JSON.stringify(req.body))
        .digest('hex');

      // Compare our HMAC with your HMAC
      if (
        !crypto.timingSafeEqual(
          Buffer.from(hmac),
          Buffer.from(req.get('Authorization-Hmac')),
        )
      ) {
        return false;
      }
      return true;
    } catch (err) {
      return false;
    }
  }

  private async saveLogError(err, data, group) {
    try {
      console.log(`[ERROR] ${group}`);
      await this.logsService.create({
        group,
        message: err?.message || 'Unknown error',
        meta: JSON.stringify({
          data: data,
        }),
      });
    } catch (err) {}
  }
}
