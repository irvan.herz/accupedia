import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { BadRequestException, Body, Controller, Post } from '@nestjs/common';
import { LogsService } from 'src/logs/logs.service';

@Controller('accurate/webhooks')
export class AccurateWebhooksController {
  constructor(
    private readonly logsService: LogsService,
    private readonly amqpConnection: AmqpConnection,
  ) {}

  @Post('receive')
  receiveItemQuantity(@Body() body) {
    try {
      if (!this.verify())
        throw new BadRequestException('Could not verify this webhook');
      body.map((item) => {
        if (item.type === 'ITEM_QUANTITY') {
          this.amqpConnection.publish(
            'companion',
            'companion.accurate.webhooks.item-quantity',
            {
              data: item,
            },
          );
        }
      });
    } catch (err) {
      this.saveLogError(err, body, 'accurate.webhook.receive.item-quantity');
    }
    return 'OK';
  }

  private async verify() {
    try {
      // const shouldVerify = this.configSvc.get<string>(
      //   'TOKOPEDIA_WEBHOOK_SHOULDVERIFY',
      // );
      // if (shouldVerify === '0') return true;
      return true;
    } catch (err) {
      return false;
    }
  }

  private async saveLogError(err, data, group) {
    try {
      console.log(`[ERROR] ${group}`);
      await this.logsService.create({
        group,
        message: err?.message || 'Unknown error',
        meta: JSON.stringify({
          data: data,
        }),
      });
    } catch (err) {}
  }
}
