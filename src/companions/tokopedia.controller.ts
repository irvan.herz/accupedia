import {
  BadRequestException,
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { ConfigurationsService } from 'src/configurations/configurations.service';
import {
  CreateTokopediaDto,
  TokopediaFilterDto,
  UpdateTokopediaDto,
} from './dto/tokopedia.dto';
import { TokopediaApiService } from './tokopedia-api.service';
import { TokopediaService } from './tokopedia.service';

@Controller('tokopedia')
export class TokopediaController {
  constructor(
    private readonly tokopediaSvc: TokopediaService,
    private readonly tokopediaApiSvc: TokopediaApiService,
    private readonly configSvc: ConfigurationsService,
  ) {}

  @Post()
  async create(@Body() payload: CreateTokopediaDto) {
    const data = await this.tokopediaSvc.create(payload as any);
    return { data };
  }

  @Get()
  async findByQuery(@Query() filter: TokopediaFilterDto) {
    const [data, numItems] = await this.tokopediaSvc.findByQuery(filter);
    const numPages = Math.ceil(numItems / filter.limit);
    const meta = {
      page: filter.page,
      limit: filter.limit,
      numItems: numItems,
      numPages,
    };
    return { data, meta };
  }

  @Patch(':id')
  async update(@Param('id') id: number, @Body() payload: UpdateTokopediaDto) {
    await this.tokopediaSvc.updateById(id, payload as any);
    return;
  }

  @Post(':id/setup')
  async setup(@Param('id') id: number) {
    let companion = await this.tokopediaSvc.findById(id);
    if (!companion) throw new NotFoundException();
    companion = await this.tokopediaSvc.auditCredentials(companion);
    if (!companion) throw new BadRequestException();
    const rpk = await this.tokopediaApiSvc.registerPublicKey(companion);
    console.log(rpk);
    return;
  }

  @Get(':id/x/orders/details')
  async findOrderDetailsByInv(@Param('id') id, @Query('id') orderId: any) {
    let companion = await this.tokopediaSvc.findById(id);
    if (!companion) throw new NotFoundException();
    companion = await this.tokopediaSvc.auditCredentials(companion);
    const order = await this.tokopediaApiSvc.getSingleOrder(companion, orderId);
    const content = order.data.encryption.content;
    const secret = order.data.encryption.secret;
    const secretContent = await this.tokopediaSvc.decryptContent(
      secret,
      content,
    );
    return { data: order, secretContent };
  }

  @Get(':id/x/orders/:orderId')
  async findOrderDetails(@Param('id') id, @Param('orderId') orderId: any) {
    let companion = await this.tokopediaSvc.findById(id);
    if (!companion) throw new NotFoundException();
    companion = await this.tokopediaSvc.auditCredentials(companion);
    const order = await this.tokopediaApiSvc.getSingleOrder(companion, orderId);
    const content = order.data.encryption.content;
    const secret = order.data.encryption.secret;
    const secretContent = await this.tokopediaSvc.decryptContent(
      secret,
      content,
    );
    return { data: order, secretContent };
  }

  @Post(':id/x/trigger-webhook')
  async triggerWebhook(@Param('id') id, @Body() payload) {
    let companion = await this.tokopediaSvc.findById(id);
    if (!companion) throw new NotFoundException();
    companion = await this.tokopediaSvc.auditCredentials(companion);
    const result = await this.tokopediaApiSvc.triggerWebhook(
      companion,
      payload,
    );
    return { data: result };
  }
}
