import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import * as FormData from 'form-data';
import { readFile } from 'fs/promises';
import * as path from 'path';
import { firstValueFrom } from 'rxjs';
import { Repository } from 'typeorm';
import { Tokopedia } from './entities/tokopedia.entity';
import {
  TriggerWebhookDto,
  UpdateStockItemDto,
} from './interfaces/tokopedia.interface';

@Injectable()
export class TokopediaApiService {
  constructor(
    @InjectRepository(Tokopedia)
    private readonly tokopediaRepo: Repository<Tokopedia>,
    private readonly httpSvc: HttpService,
    private readonly configSvc: ConfigService,
  ) {}

  async grantAccessToken(companion: Tokopedia) {
    const authClientId = companion.clientId;
    const authClientSecret = companion.clientSecret;
    const authSecretBase64 = Buffer.from(
      `${authClientId}:${authClientSecret}`,
    ).toString('base64');

    const { data } = await firstValueFrom(
      this.httpSvc.post<any>(
        `https://accounts.tokopedia.com/token?grant_type=client_credentials`,
        {},
        {
          headers: {
            Authorization: `Basic ${authSecretBase64}`,
          },
          validateStatus(status) {
            return status < 500;
          },
        },
      ),
    );
    return data;
  }

  async getSingleOrder(
    companion: Tokopedia,
    invoiceOrOrderId: string | number,
  ) {
    const params = {} as any;
    if (`${invoiceOrOrderId}`.startsWith('INV'))
      params.invoice_num = invoiceOrOrderId;
    else params.order_id = invoiceOrOrderId;

    const { data } = await firstValueFrom(
      this.httpSvc.get<any>(
        `https://fs.tokopedia.net/v2/fs/${companion.appId}/order`,
        {
          params,
          headers: {
            Authorization: `Bearer ${companion.accessToken}`,
          },
          validateStatus(status) {
            return status < 500;
          },
        },
      ),
    );
    return data;
  }

  async updateStock(companion: Tokopedia, payload: UpdateStockItemDto[]) {
    const { data } = await firstValueFrom(
      this.httpSvc.post<any>(
        `https://fs.tokopedia.net/inventory/v1/fs/${companion.appId}/stock/update?shop_id=${companion.shopId}`,
        payload,
        {
          headers: {
            Authorization: `Bearer ${companion.accessToken}`,
          },
          validateStatus(status) {
            return status < 500;
          },
        },
      ),
    );
    return data;
  }

  async triggerWebhook(companion: Tokopedia, payload: TriggerWebhookDto) {
    const { data } = await firstValueFrom(
      this.httpSvc.post<any>(
        `https://fs.tokopedia.net/v1/fs/${companion.appId}/trigger`,
        payload,
        {
          headers: {
            Authorization: `Bearer ${companion.accessToken}`,
          },
          validateStatus(status) {
            return status < 500;
          },
        },
      ),
    );
    return data;
  }

  async registerPublicKey(companion: Tokopedia) {
    try {
      const publicKeyPath = path.resolve('./private/tokopedia_public_key.pub');
      const publicKey = await readFile(publicKeyPath);
      const payload = new FormData();
      payload.append('public_key', publicKey);
      const { data } = await firstValueFrom(
        this.httpSvc.post<any>(
          `https://fs.tokopedia.net/v1/fs/${companion.appId}/register?upload=1`,
          payload,
          {
            headers: {
              Authorization: `Bearer ${companion.accessToken}`,
            },
            validateStatus(status) {
              return status < 500;
            },
          },
        ),
      );
      return data;
    } catch (err) {
      console.log(err);
    }
  }
}

// curl -X POST \
//   'https://fs.tokopedia.net/v1/fs/18262/register?upload=1' \
//   -H 'Authorization: Bearer c:CqoN6BNQQqWyqT4F0M7tNA'
//   -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
//   -F public_key=@/home/ivn/Documents/accupedia/private/tokopedia_public_key.pub
