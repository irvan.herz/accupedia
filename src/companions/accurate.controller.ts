import {
  BadRequestException,
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Patch,
  Query,
  Res,
  ServiceUnavailableException,
} from '@nestjs/common';
import * as dayjs from 'dayjs';
import { AccurateApiService } from './accurate-api.service';
import { AccurateService } from './accurate.service';
import { AccurateFilterDto } from './dto/accurate.dto';
import {
  ListCustomerDto,
  ListGlAccountDto,
  ListWarehouseDto,
} from './interfaces/accurate.interface';

@Controller('accurate')
export class AccurateController {
  constructor(
    private readonly accurateSvc: AccurateService,
    private readonly accurateApiSvc: AccurateApiService,
  ) {}

  @Get()
  async findByQuery(@Query() filter: AccurateFilterDto) {
    const [data, numItems] = await this.accurateSvc.findByQuery(filter);
    const numPages = Math.ceil(numItems / filter.limit);
    const meta = {
      page: filter.page,
      limit: filter.limit,
      numItems: numItems,
      numPages,
    };
    return { data, meta };
  }

  @Get('authorize')
  authorize(@Res() res) {
    const redirUri = this.accurateApiSvc.getAuthorizeUrl();
    return res.redirect(redirUri);
  }

  @Get('oauth-callback')
  async oauthCallback(@Query('code') code: string) {
    const grant = await this.accurateApiSvc.grantAccessToken(code);
    if (!grant?.access_token)
      throw new BadRequestException('Gagal melakukan autentikasi');
    const companion = await this.accurateSvc.findByEmail(grant.user.email);
    console.log('grant', grant);

    //cek jika perl link ulang
    if (companion) {
      await this.accurateSvc.updateById(companion.id, {
        email: grant.user.email,
        accessToken: grant.access_token,
        refreshToken: grant.refresh_token,
        accessTokenExpiredAt: dayjs().add(grant.expires_in, 'seconds').toDate(),
      });
    } else {
      await this.accurateSvc.create({
        email: grant.user.email,
        accessToken: grant.access_token,
        refreshToken: grant.refresh_token,
        accessTokenExpiredAt: dayjs().add(grant.expires_in, 'seconds').toDate(),
      });
    }
    return 'Link akun accurate berhasil';
  }

  @Patch(':id')
  async updateById(@Param('id') id: number, @Body() payload) {
    const companion = await this.accurateSvc.findById(id);
    if (!companion) throw new NotFoundException();
    const shouldOpenDb =
      companion.databaseId != +payload.databaseId && payload.databaseId;

    if (shouldOpenDb) {
      const openedDb = await this.accurateApiSvc.openDb(
        companion,
        payload.databaseId,
      );
      if (!openedDb.s) {
        const message =
          openedDb?.d?.[0] || openedDb?.error_description || 'Unknown error';
        throw new BadRequestException(`Failed to open DB: ${message}`);
      }
      payload.host = openedDb.host;
      payload.sessionId = openedDb.session;
    }
    await this.accurateSvc.updateById(id, payload);
    return;
  }

  @Get(':id/x/databases')
  async getDatabases(@Param('id') id: number) {
    const companion = await this.accurateSvc.findById(id);
    const result = await this.accurateApiSvc.listDb(companion);
    console.log(result);

    if (result?.s !== true)
      throw new ServiceUnavailableException('Error loading accurate DB');
    return {
      data: result?.d || [],
    };
  }

  @Get(':id/x/glaccounts')
  async getGlAccounts(
    @Param('id') id: number,
    @Query('q') q,
    @Query('page') page,
  ) {
    const query = {
      fields: 'accountType,name,no,id',
    } as ListGlAccountDto;
    if (q)
      query.filter = {
        keywords: {
          op: 'CONTAIN',
          val: [q],
        },
      };
    if (page)
      query.sp = {
        page: +page,
      };
    const companion = await this.accurateSvc.findById(id);
    const result = await this.accurateApiSvc.listGlAccounts(companion, query);

    if (result?.s !== true)
      throw new ServiceUnavailableException(
        'Error loading accurate GL accounts',
      );
    return {
      data: result?.d || [],
      meta: result?.sp || {},
    };
  }

  @Get(':id/x/warehouses')
  async getWarehouses(
    @Param('id') id: number,
    @Query('q') q,
    @Query('page') page,
  ) {
    const query = {} as ListWarehouseDto;
    if (q)
      query.filter = {
        keywords: {
          op: 'CONTAIN',
          val: [q],
        },
      };
    if (page)
      query.sp = {
        page: +page,
      };
    const companion = await this.accurateSvc.findById(id);
    const result = await this.accurateApiSvc.listWarehouses(companion, query);

    if (result?.s !== true)
      throw new ServiceUnavailableException(
        'Error loading accurate warehouses',
      );
    return {
      data: result?.d || [],
      meta: result?.sp || {},
    };
  }

  @Get(':id/x/branches')
  async getBranches(
    @Param('id') id: number,
    @Query('q') q,
    @Query('page') page,
  ) {
    const query = {} as ListWarehouseDto;
    if (q)
      query.filter = {
        keywords: {
          op: 'CONTAIN',
          val: [q],
        },
      };
    if (page)
      query.sp = {
        page: +page,
      };
    const companion = await this.accurateSvc.findById(id);
    const result = await this.accurateApiSvc.listBranches(companion, query);
    console.log(result);

    if (result?.s !== true)
      throw new ServiceUnavailableException('Error loading accurate branches');
    return {
      data: result?.d || [],
      meta: result?.sp || {},
    };
  }

  @Get(':id/x/customers')
  async getCustomers(
    @Param('id') id: number,
    @Query('q') q,
    @Query('page') page,
  ) {
    const query = {
      fields: 'name,customerNo,id,number,email',
    } as ListCustomerDto;
    if (q)
      query.filter = {
        keywords: {
          op: 'CONTAIN',
          val: [q],
        },
      };
    if (page)
      query.sp = {
        page: +page,
      };
    const companion = await this.accurateSvc.findById(id);
    const result = await this.accurateApiSvc.listCustomers(companion, query);
    console.log(result);

    if (result?.s !== true)
      throw new ServiceUnavailableException('Error loading accurate branches');
    return {
      data: result?.d || [],
      meta: result?.sp || {},
    };
  }

  @Get(':id')
  async findById(@Param('id') id: number) {
    const accurate = await this.accurateSvc.findById(id);
    if (!accurate) throw new NotFoundException();
    return {
      data: accurate,
    };
  }
}
