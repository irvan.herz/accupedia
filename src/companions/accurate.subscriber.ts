import { RabbitSubscribe } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';
import * as dayjs from 'dayjs';
import { Log } from 'src/logs/entities/log.entity';
import { LogsService } from 'src/logs/logs.service';
import { AccurateApiService } from './accurate-api.service';
import { AccurateService } from './accurate.service';
import {
  DetailItem,
  SaveSalesInvoiceDto,
  SaveSalesReceiptDto,
} from './interfaces/accurate.interface';
import { TokopediaApiService } from './tokopedia-api.service';
import { TokopediaService } from './tokopedia.service';

@Injectable()
export class AccurateSubscriber {
  constructor(
    private readonly logsSvc: LogsService,
    private readonly tokopediaSvc: TokopediaService,
    private readonly tokopediaApiSvc: TokopediaApiService,
    private readonly accurateSvc: AccurateService,
    private readonly accurateApiSvc: AccurateApiService,
  ) {}

  @RabbitSubscribe({
    name: 'processCreateSalesAccurate',
    exchange: 'companion',
    routingKey: 'companion.tokopedia.webhooks.order-notification',
  })
  public async processCreateSalesAccurate(payload: any) {
    console.log('processCreateSalesAccurate');
    const { data } = payload;
    try {
      const shopId = data.shop_id;
      let companion = await this.tokopediaSvc.findAccurateByShopId(shopId);
      if (!companion)
        throw new Error('Webhook diabaikan. Link accurate tidak ditemukan');
      if (
        !companion.accessToken ||
        !companion.refreshToken ||
        !companion.databaseId ||
        !companion.host ||
        !companion.defaultCustomerNo ||
        !companion.defaultDiscountAccountNo
      ) {
        throw new Error(
          'Gagal memproses webhook. Konfigurasi accurate belum siap',
        );
      }

      let decryptedContent = {} as any;
      try {
        const secretContent = data.encryption.content;
        const secretPhrase = data.encryption.secret;
        decryptedContent = await this.tokopediaSvc.decryptContent(
          secretPhrase,
          secretContent,
        );
      } catch (err) {}

      companion = await this.accurateSvc.auditCredentials(companion);

      const invoiceDto: SaveSalesInvoiceDto = {
        number: data.invoice_ref_num,
        transDate: dayjs(data.payment_date).format('DD/MM/YYYY'),
        description: `${decryptedContent?.recipient?.name || '***'} (${
          decryptedContent?.recipient?.phone || '***'
        }), ${data.logistics?.shipping_agency || 'Unknown shipping agent'} (${
          data.logistics?.service_type || 'Unknown service type'
        }), ${data.invoice_ref_num}`,
        toAddress: decryptedContent?.recipient?.address?.address_full || '-',
        customerNo: companion.defaultCustomerNo,
        detailItem: data.products.map(
          (prod: any) =>
            ({
              itemNo: prod.sku,
              quantity: prod.quantity,
              unitPrice: prod.price,
              warehouseName: companion.defaultWarehouseName,
            }) as DetailItem,
        ),
      };
      const invoice = await this.accurateApiSvc.saveSalesInvoice(
        companion,
        invoiceDto,
      );
      if (!invoice?.s) {
        const message =
          invoice?.d?.[0] || invoice?.error_description || 'Unknown error';
        if (!message.startsWith('Sudah ada data lain'))
          throw new Error(`Sales invoice gagal disinkronisasi: ${message}`);
      }

      const receiptDiscount = Math.round(
        (data.amt.ttl_product_price * 11.8) / 100,
      );
      const receiptDto: SaveSalesReceiptDto = {
        number: data.invoice_ref_num,
        transDate: dayjs(data.payment_date).format('DD/MM/YYYY'),
        customerNo: companion.defaultCustomerNo,
        // branchId: '',
        // branchName: '',
        description: '',
        bankNo: companion.defaultBankNo,
        chequeAmount: data.amt.ttl_product_price - receiptDiscount,
        detailInvoice: [
          {
            invoiceNo: data.invoice_ref_num,
            paymentAmount: data.amt.ttl_product_price,
            detailDiscount: [
              {
                accountNo: companion.defaultDiscountAccountNo,
                amount: receiptDiscount,
              },
            ],
          },
        ],
      };
      const receipt = await this.accurateApiSvc.saveSalesReceipt(
        companion,
        receiptDto,
      );
      if (!receipt?.s) {
        const message =
          receipt?.d?.[0] || receipt?.error_description || 'Unknown error';
        throw new Error(`Sales receipt gagal disinkronisasi: ${message}`);
      }

      return true;
    } catch (err) {
      await this.saveLog({
        group: 'companion.tokopedia.webhooks.order-notification',
        message: err?.message || 'Unknown error',
        meta: {
          data,
        },
      });
    }
  }

  @RabbitSubscribe({
    name: 'processDeleteSalesAccurate',
    exchange: 'companion',
    routingKey: 'companion.tokopedia.webhooks.order-status',
  })
  public async processDeleteSalesAccurate(payload: any) {
    console.log('processDeleteSalesAccurate');
    const { data } = payload;
    try {
      if (![3, 5, 6, 10, 15].includes(data.order_status)) return true;
      let tokopedia = await this.tokopediaSvc.findByShopId(data.shop_id);
      if (!tokopedia)
        throw new Error(
          `Tidak ada link tokopedia dengan shop id ${data.shop_id}`,
        );
      if (!tokopedia.accurateId)
        throw new Error(
          `Tidak ada link accurate untuk shop id ${data.shop_id}`,
        );
      let accurate = await this.accurateSvc.findById(tokopedia.accurateId);
      if (!accurate)
        throw new Error(`Link accurate ${tokopedia.accurateId} tidak valid`);

      tokopedia = await this.tokopediaSvc.auditCredentials(tokopedia);
      accurate = await this.accurateSvc.auditCredentials(accurate);

      const details = await this.tokopediaApiSvc.getSingleOrder(
        tokopedia,
        data.order_id,
      );
      // console.log(details);

      const invoiceId = details?.data?.invoice_number;
      if (!invoiceId)
        throw new Error('Gagal mendapatkan detail pesanan tokopedia');

      const listSr = await this.accurateApiSvc.listSalesReceipts(accurate, {
        fields: 'id',
        filter: {
          number: { op: 'EQUAL', val: invoiceId },
        },
      });
      // console.log(listSr);
      const deleteSrTarget = listSr?.d?.[0];
      if (!deleteSrTarget)
        throw new Error(`Tidak ditemukan data sales receipt ${invoiceId}`);
      const deleteSr = await this.accurateApiSvc.deleteSalesReceipt(
        accurate,
        deleteSrTarget,
      );
      if (!deleteSr?.s)
        throw new Error(`Gagal menghapus sales receipt ${invoiceId}`);

      const listSi = await this.accurateApiSvc.listSalesInvoices(accurate, {
        fields: 'id',
        filter: {
          number: { op: 'EQUAL', val: invoiceId },
        },
      });
      // console.log(listSi);
      const deleteSiTarget = listSi?.d?.[0];
      if (!deleteSiTarget)
        throw new Error(`Tidak ditemukan data sales invoice ${invoiceId}`);
      const deleteSi = await this.accurateApiSvc.deleteSalesInvoice(
        accurate,
        deleteSiTarget,
      );
      // console.log(deleteSi);

      if (!deleteSi?.s)
        throw new Error(`Gagal menghapus sales invoice ${invoiceId}`);
      return true;
    } catch (err) {
      await this.saveLog({
        group: 'companion.tokopedia.webhooks.order-status',
        message: err?.message || 'Unknown error',
        meta: {
          data,
        },
      });
    }
  }

  private async saveLog(payload: Partial<Log>) {
    try {
      await this.logsSvc.create(payload);
    } catch (err) {}
  }
}
