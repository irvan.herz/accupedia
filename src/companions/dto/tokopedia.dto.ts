import { PartialType } from '@nestjs/mapped-types';
import { IsNumber, IsOptional, IsString } from 'class-validator';

export class TokopediaFilterDto {
  @IsOptional()
  @IsString()
  search: string;
  @IsOptional()
  @IsNumber()
  page?: number = 1;
  @IsOptional()
  @IsNumber()
  limit?: number = 100;
  @IsOptional()
  @IsString()
  sortBy?: string = 'createdAt';
  @IsOptional()
  @IsString()
  sortOrder?: string = 'desc';
}

export class CreateTokopediaDto {
  @IsString()
  email: string;
  @IsOptional()
  @IsString()
  appId?: string;
  @IsOptional()
  @IsString()
  clientId?: string;
  @IsOptional()
  @IsString()
  clientSecret?: string;
  @IsOptional()
  @IsString()
  shopId?: string;
  @IsOptional()
  @IsNumber()
  accurateId?: number;
}

export class UpdateTokopediaDto extends PartialType(CreateTokopediaDto) {}
