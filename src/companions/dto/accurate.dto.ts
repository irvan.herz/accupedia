import { PartialType } from '@nestjs/mapped-types';
import { IsNumber, IsOptional, IsString } from 'class-validator';

export class AccurateFilterDto {
  @IsOptional()
  @IsString()
  search: string;
  @IsOptional()
  @IsNumber()
  page?: number = 1;
  @IsOptional()
  @IsNumber()
  limit?: number = 100;
  @IsOptional()
  @IsString()
  sortBy?: string = 'createdAt';
  @IsOptional()
  @IsString()
  sortOrder?: string = 'desc';
}

export class CreateAccurateDto {
  @IsString()
  email: string;
  @IsOptional()
  @IsString()
  accessToken?: string;
  @IsOptional()
  @IsString()
  refreshToken?: string;
  @IsOptional()
  @IsString()
  host?: string;
  @IsOptional()
  @IsString()
  sessionId?: string;
  @IsOptional()
  @IsString()
  defaultCustomerNo?: string;
  @IsOptional()
  @IsString()
  defaultBankNo?: string;
  @IsOptional()
  @IsString()
  defaultDiscountAccountNo?: string;
  @IsOptional()
  @IsString()
  defaultWarehouseId?: string;
  @IsOptional()
  @IsString()
  defaultWarehouseName?: string;
  @IsOptional()
  @IsNumber()
  databaseId?: number;
  @IsOptional()
  @IsString()
  syncStockStatus?: string;
}

export class UpdateAccurateDto extends PartialType(CreateAccurateDto) {}
