import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as dayjs from 'dayjs';
import { ILike, Repository } from 'typeorm';
import { AccurateApiService } from './accurate-api.service';
import { AccurateFilterDto } from './dto/accurate.dto';
import { Accurate } from './entities/accurate.entity';

@Injectable()
export class AccurateService {
  constructor(
    @InjectRepository(Accurate)
    private readonly accurateRepo: Repository<Accurate>,
    private readonly accurateApiSvc: AccurateApiService,
  ) {}

  async create(payload: Partial<Accurate>) {
    const result = await this.accurateRepo.save(payload);
    return result;
  }

  async findByQuery(filter: AccurateFilterDto) {
    const take = filter.limit || 10;
    const page = filter.page || 1;
    const skip = (page - 1) * take;
    const result = this.accurateRepo.findAndCount({
      where: {
        email: filter?.search ? ILike(`%${filter.search}%`) : undefined,
      },
      skip,
      take,
      order: { [filter.sortBy]: filter.sortOrder },
    });
    return result;
  }

  async findById(id: number) {
    const result = await this.accurateRepo.findOneBy({ id });
    return result;
  }

  async findByDatabaseId(databaseId: number) {
    const result = await this.accurateRepo.findOneBy({ databaseId });
    return result;
  }

  async findByEmail(email: string) {
    const result = await this.accurateRepo.findOneBy({ email });
    return result;
  }

  async updateById(id: number, payload: Partial<Accurate>) {
    const result = await this.accurateRepo.update(id, payload);
    return result;
  }

  async deleteById(id: number) {
    const result = await this.accurateRepo.delete(id);
    return result.affected;
  }

  async auditCredentials(companion: Accurate) {
    const expiredAt = dayjs(companion.accessTokenExpiredAt);
    const now = dayjs().add(60, 'seconds');
    if (expiredAt.isBefore(now)) {
      const refreshedToken = await this.accurateApiSvc.refreshToken(companion);
      if (!refreshedToken?.access_token)
        throw new Error(`Refresh token failed ${companion.refreshToken}`);
      companion.accessToken = refreshedToken.access_token;
      companion.refreshToken = refreshedToken.refresh_token;
      companion.accessTokenExpiredAt = dayjs()
        .add(refreshedToken.expires_in, 'seconds')
        .toDate();
      await this.accurateRepo.save(companion);
    }
    const sessionStatus = await this.accurateApiSvc.checkDbSession(companion);
    if (!sessionStatus?.s) {
      const message =
        sessionStatus?.d?.[0] ||
        sessionStatus?.error_description ||
        'Unknown error';
      throw new Error(`Failed check DB status: ${message}`);
    }
    if (!sessionStatus?.d) {
      const openedDb = await this.accurateApiSvc.openDb(
        companion,
        companion.databaseId,
      );
      if (!openedDb.s) {
        const message =
          openedDb?.d?.[0] || openedDb?.error_description || 'Unknown error';
        throw new Error(`Failed open DB ${message}`);
      }
      companion.host = openedDb.host;
      companion.sessionId = openedDb.session;
      await this.accurateRepo.save(companion);
    }
    return companion;
  }
}
