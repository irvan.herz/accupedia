import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigurationsModule } from 'src/configurations/configurations.module';
import { LogsModule } from 'src/logs/logs.module';
import { SharedRabbitMQModule } from 'src/shared/shared-rabbitmq.module';
import { AccurateApiService } from './accurate-api.service';
import { AccurateWebhooksController } from './accurate-webhooks.controller';
import { AccurateController } from './accurate.controller';
import { AccurateService } from './accurate.service';
import { AccurateSubscriber } from './accurate.subscriber';
import { Accurate } from './entities/accurate.entity';
import { Tokopedia } from './entities/tokopedia.entity';
import { TokopediaApiService } from './tokopedia-api.service';
import { TokopediaWebhooksController } from './tokopedia-webhooks.controller';
import { TokopediaController } from './tokopedia.controller';
import { TokopediaService } from './tokopedia.service';
import { TokopediaSubscriber } from './tokopedia.subscriber';

@Module({
  imports: [
    TypeOrmModule.forFeature([Accurate, Tokopedia]),
    ConfigModule,
    ConfigurationsModule,
    HttpModule,
    LogsModule,
    SharedRabbitMQModule,
  ],
  controllers: [
    AccurateWebhooksController,
    AccurateController,
    TokopediaWebhooksController,
    TokopediaController,
  ],
  providers: [
    AccurateService,
    AccurateApiService,
    TokopediaService,
    TokopediaApiService,
    TokopediaSubscriber,
    AccurateSubscriber,
  ],
  exports: [],
})
export class CompanionsModule {}
