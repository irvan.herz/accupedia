import { RabbitSubscribe } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';
import { Log } from 'src/logs/entities/log.entity';
import { LogsService } from 'src/logs/logs.service';
import { AccurateService } from './accurate.service';
import { UpdateStockItemDto } from './interfaces/tokopedia.interface';
import { TokopediaApiService } from './tokopedia-api.service';
import { TokopediaService } from './tokopedia.service';

@Injectable()
export class TokopediaSubscriber {
  constructor(
    private readonly logsSvc: LogsService,
    private readonly accurateSvc: AccurateService,
    private readonly tokopediaSvc: TokopediaService,
    private readonly tokopediaApiSvc: TokopediaApiService,
  ) {}

  @RabbitSubscribe({
    name: 'processUpdateStockTokopedia',
    exchange: 'companion',
    routingKey: 'companion.accurate.webhooks.item-quantity',
  })
  public async processUpdateStockTokopedia(payload: any) {
    console.log('processUpdateStockTokopedia');
    const { data } = payload;
    try {
      const databaseId = data.databaseId;
      let accurate = await this.accurateSvc.findByDatabaseId(databaseId);
      if (!accurate)
        throw new Error(`Tidak ada link accurate untuk database ${databaseId}`);
      let tokopedia = await this.tokopediaSvc.findByAccurateId(accurate.id);
      if (!tokopedia)
        throw new Error(
          `Tidak ada link tokopedia untuk akun accurate ${accurate.id}`,
        );
      if (accurate.syncStockStatus !== 'enabled') return true;

      accurate = await this.accurateSvc.auditCredentials(accurate);
      tokopedia = await this.tokopediaSvc.auditCredentials(tokopedia);

      const items = data?.data || [];
      const updatePayload = items.reduce((a, c) => {
        if (c.warehouseId == accurate.defaultWarehouseId)
          a.push({
            sku: c.itemNo,
            new_stock: c.quantity,
          });
        return a;
      }, [] as UpdateStockItemDto[]);

      if (!updatePayload.length) return true;
      const response = await this.tokopediaApiSvc.updateStock(
        tokopedia,
        updatePayload,
      );

      console.log(response);

      if (!response?.data?.failed_rows && !response?.data?.succeed_rows) {
        const msg = response?.header?.messages || 'Unknown error';
        throw new Error(`Gagal melakukan sinkronisasi ke Tokopedia: ${msg}`);
      }
      if (response?.data?.failed_rows) {
        await this.saveLog({
          group: 'companion.accurate.webhook.item-quantity',
          message: 'Ada SKU yang stoknya gagal diupdate ke tokopedia',
          meta: {
            data,
            response,
          },
        });
      }

      return true;
    } catch (err) {
      await this.saveLog({
        group: 'companion.accurate.webhook.item-quantity',
        message: err?.message || 'Unknown error',
        meta: {
          data,
        },
      });
    }
  }

  private async saveLog(payload: Partial<Log>) {
    try {
      await this.logsSvc.create(payload);
    } catch (err) {}
  }
}
