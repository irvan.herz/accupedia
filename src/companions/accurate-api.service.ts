import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import { firstValueFrom } from 'rxjs';
import { Repository } from 'typeorm';
import { Accurate } from './entities/accurate.entity';
import {
  DeleteSalesInvoiceDto,
  DeleteSalesReceiptDto,
  ListBranchDto,
  ListCustomerDto,
  ListGlAccountDto,
  ListSalesInvoiceDto,
  ListSalesReceiptDto,
  ListWarehouseDto,
  SaveSalesInvoiceDto,
  SaveSalesReceiptDto,
} from './interfaces/accurate.interface';

@Injectable()
export class AccurateApiService {
  constructor(
    @InjectRepository(Accurate)
    private readonly accurateRepo: Repository<Accurate>,
    private readonly httpSvc: HttpService,
    private readonly configSvc: ConfigService,
  ) {}

  getAuthorizeUrl() {
    const authClientId = this.configSvc.get<string>('ACCURATE_CLIENT_ID');
    const authRedirectUri = this.configSvc.get<string>('ACCURATE_REDIRECT_URI');
    const authScope = this.configSvc.get<string>('ACCURATE_SCOPE');
    return `https://account.accurate.id/oauth/authorize?client_id=${authClientId}&response_type=code&redirect_uri=${authRedirectUri}&scope=${authScope}`;
  }

  async grantAccessToken(authCode: string) {
    const redirectUri = this.configSvc.get<string>('ACCURATE_REDIRECT_URI');
    const authClientId = this.configSvc.get<string>('ACCURATE_CLIENT_ID');
    const authClientSecret = this.configSvc.get<string>(
      'ACCURATE_CLIENT_SECRET',
    );
    const authSecretBase64 = Buffer.from(
      `${authClientId}:${authClientSecret}`,
    ).toString('base64');

    const payload = new URLSearchParams();
    payload.set('code', authCode);
    payload.set('grant_type', 'authorization_code');
    payload.set('redirect_uri', redirectUri);

    const { data } = await firstValueFrom(
      this.httpSvc.post<any>(
        `https://account.accurate.id/oauth/token`,
        payload,
        {
          headers: {
            Authorization: `Basic ${authSecretBase64}`,
          },
        },
      ),
    );
    return data;
  }

  async refreshToken(user: Accurate) {
    const authClientId = this.configSvc.get<string>('ACCURATE_CLIENT_ID');
    const authClientSecret = this.configSvc.get<string>(
      'ACCURATE_CLIENT_SECRET',
    );
    const authSecretBase64 = Buffer.from(
      `${authClientId}:${authClientSecret}`,
    ).toString('base64');

    const payload = new URLSearchParams();
    payload.set('grant_type', 'refresh_token');
    payload.set('refresh_token', user.refreshToken);

    const { data } = await firstValueFrom(
      this.httpSvc.post<any>(
        `https://account.accurate.id/oauth/token`,
        payload,
        {
          headers: {
            Authorization: `Basic ${authSecretBase64}`,
          },
          validateStatus(status) {
            return status < 500;
          },
        },
      ),
    );
    return data;
  }

  async listDb(user: Accurate) {
    const { data } = await firstValueFrom(
      this.httpSvc.get<any>(`https://account.accurate.id/api/db-list.do`, {
        headers: {
          Authorization: `Bearer ${user.accessToken}`,
        },
        validateStatus(status) {
          return status < 500;
        },
      }),
    );
    return data;
  }

  async openDb(user: Accurate, id: number) {
    const { data } = await firstValueFrom(
      this.httpSvc.get<any>(`https://account.accurate.id/api/open-db.do`, {
        headers: {
          'X-SESSION-ID': user.sessionId,
          Authorization: `Bearer ${user.accessToken}`,
        },
        data: { id },
        validateStatus(status) {
          return status < 500;
        },
      }),
    );
    return data;
  }

  async checkDbSession(user: Accurate) {
    const { data } = await firstValueFrom(
      this.httpSvc.get<any>(
        `https://account.accurate.id/api/db-check-session.do`,
        {
          data: {
            session: user.sessionId,
          },
          headers: {
            'X-SESSION-ID': user.sessionId,
            Authorization: `Bearer ${user.accessToken}`,
          },
          validateStatus(status) {
            return status < 500;
          },
        },
      ),
    );
    return data;
  }

  async refreshDbSession(user: Accurate) {
    const { data } = await firstValueFrom(
      this.httpSvc.get<any>(
        `https://account.accurate.id/api/db-refresh-session.do`,
        {
          data: {
            id: user.databaseId,
            session: user.sessionId,
          },
          validateStatus(status) {
            return status < 500;
          },
        },
      ),
    );
    return data;
  }

  async saveCustomer(user: Accurate, payload: any) {
    const { data } = await firstValueFrom(
      this.httpSvc.post<any>(
        `${user.host}/accurate/api/customer/save.do`,
        payload,
        {
          headers: {
            'X-SESSION-ID': user.sessionId,
            Authorization: `Bearer ${user.accessToken}`,
          },
          validateStatus(status) {
            return status < 500;
          },
        },
      ),
    );
    return data;
  }

  async listSalesInvoices(user: Accurate, payload: ListSalesInvoiceDto) {
    const { data } = await firstValueFrom(
      this.httpSvc.get<any>(`${user.host}/accurate/api/sales-invoice/list.do`, {
        headers: {
          'X-SESSION-ID': user.sessionId,
          Authorization: `Bearer ${user.accessToken}`,
        },
        data: payload,
        validateStatus(status) {
          return status < 500;
        },
      }),
    );
    return data;
  }

  async saveSalesInvoice(user: Accurate, payload: SaveSalesInvoiceDto) {
    const { data } = await firstValueFrom(
      this.httpSvc.post<any>(
        `${user.host}/accurate/api/sales-invoice/save.do`,
        payload,
        {
          headers: {
            'X-SESSION-ID': user.sessionId,
            Authorization: `Bearer ${user.accessToken}`,
          },
          validateStatus(status) {
            return status < 500;
          },
        },
      ),
    );
    return data;
  }

  async deleteSalesInvoice(user: Accurate, payload: DeleteSalesInvoiceDto) {
    const { data } = await firstValueFrom(
      this.httpSvc.delete<any>(
        `${user.host}/accurate/api/sales-invoice/delete.do`,
        {
          headers: {
            'X-SESSION-ID': user.sessionId,
            Authorization: `Bearer ${user.accessToken}`,
          },
          data: payload,
          validateStatus(status) {
            return status < 500;
          },
        },
      ),
    );
    return data;
  }

  async saveSalesReceipt(user: Accurate, payload: SaveSalesReceiptDto) {
    const { data } = await firstValueFrom(
      this.httpSvc.post<any>(
        `${user.host}/accurate/api/sales-receipt/save.do`,
        payload,
        {
          headers: {
            'X-SESSION-ID': user.sessionId,
            Authorization: `Bearer ${user.accessToken}`,
          },
          validateStatus(status) {
            return status < 500;
          },
        },
      ),
    );
    return data;
  }

  async listSalesReceipts(user: Accurate, payload: ListSalesReceiptDto) {
    const { data } = await firstValueFrom(
      this.httpSvc.get<any>(`${user.host}/accurate/api/sales-receipt/list.do`, {
        headers: {
          'X-SESSION-ID': user.sessionId,
          Authorization: `Bearer ${user.accessToken}`,
        },
        data: payload,
        validateStatus(status) {
          return status < 500;
        },
      }),
    );
    return data;
  }

  async deleteSalesReceipt(user: Accurate, payload: DeleteSalesReceiptDto) {
    const { data } = await firstValueFrom(
      this.httpSvc.delete<any>(
        `${user.host}/accurate/api/sales-receipt/delete.do`,
        {
          headers: {
            'X-SESSION-ID': user.sessionId,
            Authorization: `Bearer ${user.accessToken}`,
          },
          data: payload,
          validateStatus(status) {
            return status < 500;
          },
        },
      ),
    );
    return data;
  }

  async listGlAccounts(user: Accurate, payload: ListGlAccountDto) {
    const { data } = await firstValueFrom(
      this.httpSvc.get<any>(`${user.host}/accurate/api/glaccount/list.do`, {
        headers: {
          'X-SESSION-ID': user.sessionId,
          Authorization: `Bearer ${user.accessToken}`,
        },
        data: payload,
        validateStatus(status) {
          return status < 500;
        },
      }),
    );
    return data;
  }

  async listWarehouses(user: Accurate, payload: ListWarehouseDto) {
    const { data } = await firstValueFrom(
      this.httpSvc.get<any>(`${user.host}/accurate/api/warehouse/list.do`, {
        headers: {
          'X-SESSION-ID': user.sessionId,
          Authorization: `Bearer ${user.accessToken}`,
        },
        data: payload,
        validateStatus(status) {
          return status < 500;
        },
      }),
    );
    return data;
  }

  async listBranches(user: Accurate, payload: ListBranchDto) {
    const { data } = await firstValueFrom(
      this.httpSvc.get<any>(`${user.host}/accurate/api/branch/list.do`, {
        headers: {
          'X-SESSION-ID': user.sessionId,
          Authorization: `Bearer ${user.accessToken}`,
        },
        data: payload,
        validateStatus(status) {
          return status < 500;
        },
      }),
    );
    return data;
  }

  async listCustomers(user: Accurate, payload: ListCustomerDto) {
    const { data } = await firstValueFrom(
      this.httpSvc.get<any>(`${user.host}/accurate/api/customer/list.do`, {
        headers: {
          'X-SESSION-ID': user.sessionId,
          Authorization: `Bearer ${user.accessToken}`,
        },
        data: payload,
        validateStatus(status) {
          return status < 500;
        },
      }),
    );
    return data;
  }
}
