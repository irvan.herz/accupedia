export type TokopediaAuth = {
  accessToken: string;
  expiredAt: Date;
};

export type TriggerWebhookDto = {
  type: 'order_notification';
  order_id: string;
  url: string;
  is_encrypted: boolean;
};

export type UpdateStockItemDto = {
  bypass_update_product_status?: boolean;
  warehouse_id?: number;
  product_id?: number;
  sku?: string;
  new_stock: number;
};
