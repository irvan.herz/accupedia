export type AccurateUser = {
  host: string;
  sessionId: string;
  accessToken: string;
  refreshToken: string;
  databaseId: string;
};

export type DetailDownPayment = {
  _status?: 'delete'; //DetailStateType
  id?: number;
  invoiceNumber?: string;
  paymentAmount?: number; //Money
};

export type DetailExpense = {
  _status?: 'delete';
  accountNo?: string;
  dataClassification1Name?: string;
  dataClassification2Name?: string;
  dataClassification3Name?: string;
  dataClassification4Name?: string;
  dataClassification5Name?: string;
  dataClassification6Name?: string;
  dataClassification7Name?: string;
  dataClassification8Name?: string;
  dataClassification9Name?: string;
  dataClassification10Name?: string;
  departmentName?: string;
  expenseAmount?: number; //money
  expenseName?: string;
  expenseNotes?: string;
  id?: number;
  salesOrderNumber?: string;
  salesQuotationNumber?: string;
};

export type DetailItemSerialNumber = {
  _status?: 'delete';
  expiredDate?: Date | string;
  id?: string;
  quantity?: number;
  serialNumberNo?: string;
};

export type DetailItem = {
  unitPrice: number; //money
  _status?: 'delete';
  controlQuantity?: number; //money
  dataClassification1Name?: string;
  dataClassification2Name?: string;
  dataClassification3Name?: string;
  dataClassification4Name?: string;
  dataClassification5Name?: string;
  dataClassification6Name?: string;
  dataClassification7Name?: string;
  dataClassification8Name?: string;
  dataClassification9Name?: string;
  dataClassification10Name?: string;
  deliveryOrderNumber?: string;
  departmentName?: string;
  detailName?: string;
  detailNotes?: string;
  detailSerialNumber?: DetailItemSerialNumber[];
  id?: number;
  itemCashDiscount?: number; //money
  itemDiscPercent?: string;
  itemUnitName?: string;
  projectNo?: string;
  quantity?: number; //money
  salesOrderNumber?: string;
  salesQuotationNumber?: string;
  salesmanListNumber?: string;
  useTax1?: boolean;
  useTax2?: boolean;
  useTax3?: boolean;
  warehouseName?: string;
};

export type SaveSalesInvoiceDto = {
  customerNo: string;
  detailDownPayment?: DetailDownPayment[];
  detailExpense?: DetailExpense[];
  detailItem?: DetailItem[];
  transDate: Date | string;
  branchId?: string;
  branchName?: string;
  cashDiscPercent?: string;
  cashDiscount?: number; //money
  currencyCode?: string;
  description?: string;
  documentCode?: 'DIGUNGGUNG' | 'DOCUMENT' | 'EXPORT' | 'INVOICE';
  fiscalRate?: number; //money
  fobName?: string;
  id?: number;
  inclusiveTax?: boolean;
  inputDownPayment?: number; //money
  invoiceDp?: boolean;
  number?: string;
  orderDownPaymentNumber?: string;
  paymentTermName?: string;
  poNumber?: string;
  rate?: number; //money
  retailIdCard?: string;
  retailWpName?: string;
  reverseInvoice?: boolean;
  shipDate?: Date | string;
  shipmentName?: string;
  tax1Name?: string;
  taxDate?: Date | string;
  taxNumber?: string;
  taxType?:
    | 'BESARAN_TERTENTU'
    | 'BKN_PEMUNGUT_PPN'
    | 'DPP_NILAILAIN'
    | 'EXPBKP_TDKWJD'
    | 'EXPBKP_WJD'
    | 'EXP_JKP'
    | 'PAJAK_DIDEEMED'
    | 'PEMUNGUT_BENDAHARA_PEMERINTAH'
    | 'PEMUNGUT_PPN'
    | 'PENYERAHAN_ASSET'
    | 'PENYERAHAN_LAIN'
    | 'PPN_DIBEBASKAN';
  taxable?: boolean;
  toAddress?: string;
  typeAutoNumber?: number;
};

export type DetailInvoice = {
  invoiceNo: string;
  paymentAmount: number; //money
  _status?: 'delete';
  departmentName?: string;
  detailDiscount?: DetailDiscount[];
  id?: number;
  paidPph?: boolean;
  pphNumber?: string;
  pphTypeAutoNumber?: number;
};

export type DetailDiscount = {
  accountNo: string;
  amount: number;
  _status?: 'delete';
  departmentName?: string;
  discountNotes?: string;
  id?: number;
  projectNo?: string;
};

export type SaveSalesReceiptDto = {
  bankNo: string;
  chequeAmount: number; // money
  customerNo: string;
  detailInvoice?: DetailInvoice[];
  transDate: Date | string;
  branchId?: number;
  branchName?: string;
  chequeDate?: Date | string;
  chequeNo?: string;
  currencyCode?: string;
  description?: string;
  id?: number;
  number?: string;
  passValidateInvoiceDate?: boolean;
  paymentMethod?:
    | 'BANK_CHEQUE'
    | 'BANK_TRANSFER'
    | 'CASH_OTHER'
    | 'EDC'
    | 'OTHERS'
    | 'PAYMENT_LINK'
    | 'QRIS'
    | 'VIRTUAL_ACCOUNT';
  rate?: number; //money
  typeAutoNumber?: number;
  useCredit?: boolean;
};

export type DeleteSalesInvoiceDto = {
  id?: number;
  number?: string;
};

export type DeleteSalesReceiptDto = {
  id: number;
};

export type DetailSalesReceiptDto = {
  id: number;
};

type BasicFilterOperator =
  | 'BETWEEN'
  | 'EMPTY'
  | 'EQUAL'
  | 'GREATER_EQUAL_THAN'
  | 'GREATER_THAN'
  | 'LESS_EQUAL_THAN'
  | 'LESS_THAN'
  | 'NOT_BETWEEN'
  | 'NOT_EMPTY'
  | 'NOT_EQUAL';
type StringFilterOperator =
  | 'BETWEEN'
  | 'CONTAIN'
  | 'EMPTY'
  | 'EQUAL'
  | 'GREATER_EQUAL_THAN'
  | 'GREATER_THAN'
  | 'LESS_EQUAL_THAN'
  | 'LESS_THAN'
  | 'NOT_BETWEEN'
  | 'NOT_EMPTY'
  | 'NOT_EQUAL';
type ApprovalStatus =
  | 'APPROVED'
  | 'DRAFT'
  | 'NEXTUSER_TOAPPROVED'
  | 'REJECTED'
  | 'UNAPPROVED';

export type ListSalesInvoiceDto = {
  approvalStatusFilter?: ApprovalStatus[];
  branchFilter?: number[];
  currencyFilter?: number[];
  customerFilter?: any[]; //[{"id":50}, {"id":120}]
  dueDateFilter?: Date | string;
  approvalStatus?: ApprovalStatus;
  fields?: string; //comma separated
  // approvalStatus?: {
  //   op: BasicFilterOperator;
  //   val?: ApprovalStatus | ApprovalStatus[];
  // };
  filter?: {
    branchId?: {
      op: BasicFilterOperator;
      val?: number | number[];
    };
    currencyId?: {
      op: BasicFilterOperator;
      val?: number | number[];
    };
    customerId?: {
      op: BasicFilterOperator;
      val?: number | number[];
    };
    dueDate?: {
      op: BasicFilterOperator;
      val?: string | string[] | Date | Date[];
    };
    id?: {
      op: BasicFilterOperator;
      val?: number | number[];
      invoiceDp?: boolean;
    };
    keywords?: {
      op: StringFilterOperator;
      val?: string | string[];
    };
    lastPaymentDate?: {
      op: BasicFilterOperator;
      val?: string | string[] | Date | Date[];
    };
    lastUpdate?: {
      op: BasicFilterOperator;
      val?: string | string[] | Date | Date[];
    };
    number?: {
      op: StringFilterOperator;
      val?: string | string[];
      openingBalance?: boolean;
      outstanding?: boolean;
      overdue?: boolean;
    };
    transDate?: {
      op: BasicFilterOperator;
      val?: string | string[] | Date | Date[];
    };
  };
  sp?: {
    page?: number;
    pageSize?: number;
    sort?: string; //Contoh, jika ingin diurutkan berdasarkan nama secara ascending, lalu berdasarkan nomor secara descending maka gunakan: name|asc;no|desc
  };
};

export type ListSalesReceiptDto = {
  fields?: string; //comma separated
  filter?: {
    approvalStatus?: {
      op: BasicFilterOperator;
      val?: ApprovalStatus | ApprovalStatus[];
    };
    branchId?: {
      op: BasicFilterOperator;
      val?: number | number[];
    };
    currencyId?: {
      op: BasicFilterOperator;
      val?: number | number[];
    };
    customerId?: {
      op: BasicFilterOperator;
      val?: number | number[];
    };
    keywords?: {
      op: StringFilterOperator;
      val?: string | string[];
    };
    lastUpdate?: {
      op: BasicFilterOperator;
      val?: string | string[] | Date | Date[];
    };
    number?: {
      op: StringFilterOperator;
      val?: string | string[];
    };
    transDate?: {
      op: BasicFilterOperator;
      val?: string | string[] | Date | Date[];
    };
  };
  sp?: {
    page?: number;
    pageSize?: number;
    sort?: string; //Contoh, jika ingin diurutkan berdasarkan nama secara ascending, lalu berdasarkan nomor secara descending maka gunakan: name|asc;no|desc
  };
};

type GlAccountType =
  | 'ACCOUNT_PAYABLE'
  | 'ACCOUNT_RECEIVABLE'
  | 'ACCUMULATED_DEPRECIATION'
  | 'CASH_BANK'
  | 'COGS'
  | 'EQUITY'
  | 'EXPENSE'
  | 'FIXED_ASSET'
  | 'INVENTORY'
  | 'LONG_TERM_LIABILITY'
  | 'OTHER_ASSET'
  | 'OTHER_CURRENT_ASSET'
  | 'OTHER_CURRENT_LIABILITY'
  | 'OTHER_EXPENSE'
  | 'OTHER_INCOME'
  | 'REVENUE';

export type ListGlAccountDto = {
  fields?: string; //comma separated
  filter?: {
    accountType?: {
      op: BasicFilterOperator;
      val?: GlAccountType | GlAccountType[];
    };
    keywords?: {
      op: StringFilterOperator;
      val?: string | string[];
    };
    lastUpdate?: {
      op: BasicFilterOperator;
      val?: string | string[] | Date | Date[];
    };
  };
  sp?: {
    page?: number;
    pageSize?: number;
    sort?: string; //Contoh, jika ingin diurutkan berdasarkan nama secara ascending, lalu berdasarkan nomor secara descending maka gunakan: name|asc;no|desc
  };
};

export type ListWarehouseDto = {
  filter?: {
    keywords?: {
      op: StringFilterOperator;
      val?: string | string[];
    };
    lastUpdate?: {
      op: BasicFilterOperator;
      val?: string | string[] | Date | Date[];
    };
  };
  sp?: {
    page?: number;
    pageSize?: number;
    sort?: string; //Contoh, jika ingin diurutkan berdasarkan nama secara ascending, lalu berdasarkan nomor secara descending maka gunakan: name|asc;no|desc
  };
};

export type ListBranchDto = {
  filter?: {
    keywords?: {
      op: StringFilterOperator;
      val?: string | string[];
    };
    lastUpdate?: {
      op: BasicFilterOperator;
      val?: string | string[] | Date | Date[];
    };
  };
  sp?: {
    page?: number;
    pageSize?: number;
    sort?: string; //Contoh, jika ingin diurutkan berdasarkan nama secara ascending, lalu berdasarkan nomor secara descending maka gunakan: name|asc;no|desc
  };
};

export type ListCustomerDto = {
  fields?: string; //comma separated
  filter?: {
    branchId?: {
      op: BasicFilterOperator;
      val?: number | number[];
    };
    customerCategoryId?: {
      op: BasicFilterOperator;
      val?: number | number[];
    };
    keywords?: {
      op: StringFilterOperator;
      val?: string | string[];
    };
    lastUpdate?: {
      op: BasicFilterOperator;
      val?: string | string[] | Date | Date[];
    };
    no?: {
      op: StringFilterOperator;
      val?: string | string[];
    };
    npwpNo?: {
      op: StringFilterOperator;
      val?: string | string[];
      suspended?: boolean;
    };
  };
  sp?: {
    page?: number;
    pageSize?: number;
    sort?: string; //Contoh, jika ingin diurutkan berdasarkan nama secara ascending, lalu berdasarkan nomor secara descending maka gunakan: name|asc;no|desc
  };
};
