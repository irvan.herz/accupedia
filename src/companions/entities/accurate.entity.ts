import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Accurate {
  @PrimaryGeneratedColumn()
  id: number;
  @Column('varchar', { length: 300, nullable: true })
  email: string;
  @Column('varchar', { length: 300, nullable: true })
  accessToken: string;
  @Column('varchar', { length: 300, nullable: true })
  refreshToken: string;
  @Column('varchar', { length: 300, nullable: true })
  host: string;
  @Column('varchar', { length: 300, nullable: true })
  sessionId: string;
  @Column('int', { nullable: true })
  databaseId: number;
  @Column('varchar', { length: 300, nullable: true })
  defaultCustomerNo: string;
  @Column('varchar', { length: 300, nullable: true })
  defaultBankNo: string;
  @Column('varchar', { length: 300, nullable: true })
  defaultDiscountAccountNo: string;
  @Column('varchar', { length: 300, nullable: true })
  defaultWarehouseId: string;
  @Column('varchar', { length: 300, nullable: true })
  defaultWarehouseName: string;
  @Column('enum', { enum: ['enabled', 'disabled'], default: 'disabled' })
  syncStockStatus: string;
  @Column('timestamptz', { nullable: true })
  accessTokenExpiredAt: Date;
  @Column('timestamptz', { nullable: true })
  refreshTokenExpiredAt: Date;
  @CreateDateColumn()
  createdAt: Date;
  @UpdateDateColumn()
  updatedAt: Date;
}
