import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Tokopedia {
  @PrimaryGeneratedColumn()
  id: number;
  @Column('varchar', { length: 300, nullable: true })
  email: string;
  @Column('int', { nullable: true })
  accurateId: number;
  @Column('int', { nullable: true })
  appId: number;
  @Column('int', { nullable: true })
  shopId: number;
  @Column('varchar', { length: 300, nullable: true })
  clientId: string;
  @Column('varchar', { length: 300, nullable: true })
  clientSecret: string;
  @Column('varchar', { length: 300, nullable: true })
  accessToken: string;
  @Column('varchar', { length: 300, nullable: true })
  refreshToken: string;
  @Column('timestamptz', { nullable: true })
  accessTokenExpiredAt: Date;
  @CreateDateColumn()
  createdAt: Date;
  @UpdateDateColumn()
  updatedAt: Date;
}
