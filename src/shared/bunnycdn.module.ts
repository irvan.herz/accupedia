import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { BunnyCDNService } from './bunnycdn.service';

@Module({
  imports: [HttpModule],
  providers: [BunnyCDNService],
  exports: [BunnyCDNService],
})
export class BunnyCDNModule {}
