export class BunnyCDNUploadFileDto {
  accessKey: string;
  region: string;
  zone: string;
  path: string;
  file: Buffer;
}
