import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { firstValueFrom } from 'rxjs';
import { BunnyCDNUploadFileDto } from './dto/bunnycdn.dto';

@Injectable()
export class BunnyCDNService {
  constructor(private readonly httpSvc: HttpService) {}

  async uploadFile(payload: BunnyCDNUploadFileDto) {
    const url = new URL(
      payload.path,
      `https://${payload.region}.storage.bunnycdn.com/${payload.zone}/`,
    );
    const { data } = await firstValueFrom(
      this.httpSvc.put<any>(url.href, payload.file, {
        headers: {
          AccessKey: payload.accessKey,
          'Content-Type': 'application/octet-stream',
        },
        validateStatus(status) {
          return status < 500;
        },
      }),
    );
    return data;
  }
}
