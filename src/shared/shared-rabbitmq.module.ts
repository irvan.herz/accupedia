import { RabbitMQModule } from '@golevelup/nestjs-rabbitmq';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  controllers: [],
  imports: [
    RabbitMQModule.forRootAsync(RabbitMQModule, {
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        uri: configService.get<string>('RABBITMQ_SERVER'),
        exchanges: [
          //USERS
          {
            createExchangeIfNotExists: true,
            name: 'companion',
            type: 'topic',
            options: { autoDelete: true },
          },
        ],
      }),
    }),
  ],
  providers: [],
  exports: [RabbitMQModule],
})
export class SharedRabbitMQModule {}
