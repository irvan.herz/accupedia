import {
  Controller,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { FileInterceptor } from '@nestjs/platform-express';
import * as path from 'path';
import slugify from 'slugify';
import { BunnyCDNService } from 'src/shared/bunnycdn.service';
import { MediaService } from './media.service';

const PRODUCT_IMAGE_RESOLUTIONS = [
  {
    id: 'sm',
    width: 300,
  },
  {
    id: 'md',
    width: 640,
  },
  {
    id: 'lg',
    width: 920,
  },
];

@Controller('media')
export class MediaController {
  constructor(
    private readonly mediaSvc: MediaService,
    private readonly bunnySvc: BunnyCDNService,
    private readonly configSvc: ConfigService,
  ) {}

  @Post('upload')
  @UseInterceptors(FileInterceptor('file'))
  async upload(@UploadedFile() file: Express.Multer.File) {
    const fileOriginalName = file.originalname;
    // const fileSize = file.size
    // const fileMime = file.mimetype
    const { name: fileName } = path.parse(fileOriginalName);
    const randomPostfix = '-' + Date.now().toString();
    const adjustedFileName = slugify(fileName, {
      lower: true,
      trim: true,
      replacement: '-',
    });

    const result = [];
    for (const resolution of PRODUCT_IMAGE_RESOLUTIONS) {
      const targetFileName = `${adjustedFileName}-${resolution.id}-${randomPostfix}.jpg`;
      await this.bunnySvc.uploadFile({
        accessKey: this.configSvc.get<string>('BUNNYCDN_MEDIA_ACCESS_KEY'),
        region: this.configSvc.get<string>('BUNNYCDN_MEDIA_REGION'),
        zone: this.configSvc.get<string>('BUNNYCDN_MEDIA_ZONE'),
        path: targetFileName,
        file: file.buffer,
      });
      result.push({
        url: '',
        name: '',
        meta: {},
      });
    }

    return result;
  }
}
