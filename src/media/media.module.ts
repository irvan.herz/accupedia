import { Module } from '@nestjs/common';
import { BunnyCDNModule } from 'src/shared/bunnycdn.module';
import { MediaController } from './media.controller';
import { MediaService } from './media.service';

@Module({
  imports: [BunnyCDNModule],
  controllers: [MediaController],
  providers: [MediaService],
})
export class MediaModule {}
