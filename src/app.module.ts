import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CompanionsModule } from './companions/companions.module';
import { Accurate } from './companions/entities/accurate.entity';
import { Tokopedia } from './companions/entities/tokopedia.entity';
import { ConfigurationsModule } from './configurations/configurations.module';
import { Configuration } from './configurations/entities/configuration.entity';
import { Log } from './logs/entities/log.entity';
import { LogsModule } from './logs/logs.module';
import { OrderDetail } from './orders/entities/order-detail.entity';
import { OrderSourceInfo } from './orders/entities/order-source-info.entity';
import { Order } from './orders/entities/order.entity';
import { OrdersModule } from './orders/orders.module';
import { ProductCategory } from './products/entities/product-category.entity';
import { Product } from './products/entities/product.entity';
import { Stock } from './products/entities/stock.entity';
import { ProductsModule } from './products/products.module';
import { User } from './users/entities/user.entity';
import { UsersModule } from './users/users.module';
import { MediaModule } from './media/media.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        type: 'postgres',
        url: configService.get<string>('DATABASE_URL'),
        synchronize: true,
        // logging: true,
        entities: [
          Accurate,
          Log,
          Tokopedia,
          Configuration,
          User,
          Product,
          ProductCategory,
          Stock,
          Order,
          OrderDetail,
          OrderSourceInfo,
        ],
      }),
    }),
    HttpModule.registerAsync({
      useFactory: () => ({
        timeout: 50000,
        maxRedirects: 5,
      }),
    }),
    LogsModule,
    UsersModule,
    ConfigurationsModule,
    ProductsModule,
    OrdersModule,
    CompanionsModule,
    MediaModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
