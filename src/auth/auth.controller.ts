import {
  BadRequestException,
  Body,
  Controller,
  InternalServerErrorException,
  Post,
  UnauthorizedException,
  UseGuards,
} from '@nestjs/common';
import { CurrentUser } from 'src/shared/current-user.decorator';
import { LocalAuthGuard } from 'src/shared/local-auth.guard';
import { UsersService } from 'src/users/users.service';
import { AuthService } from './auth.service';
import { SignupWithEmailDto } from './dto/auth.dto';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly authSvc: AuthService,
    private readonly usersSvc: UsersService,
  ) {}

  @Post('/signup')
  async signupWithEmail(@Body() payload: SignupWithEmailDto) {
    try {
      const user = await this.authSvc.signup(payload);
      const auth = await this.authSvc.signin({
        id: user.id,
        username: user.username,
        email: user.email,
        role: user.role,
      });
      return {
        data: user,
        meta: auth,
      };
    } catch (err) {
      console.log(err);

      if (err?.code === '23505')
        throw new BadRequestException('Email or username already used');
      throw new InternalServerErrorException();
    }
  }

  @Post('/refresh-token')
  async refreshToken(@Body('refreshToken') rtoken: string) {
    const cred = await this.authSvc.validateRefreshToken(rtoken);
    if (!cred) throw new UnauthorizedException();
    const user = await this.usersSvc.findById(cred.id);
    if (!user) throw new UnauthorizedException();
    const auth = await this.authSvc.signin({
      id: user.id,
      username: user.username,
      email: user.email,
      role: user.role,
    });
    return {
      data: user,
      meta: {
        token: auth.token,
        refreshToken: auth.refreshToken,
      },
    };
  }

  @UseGuards(LocalAuthGuard)
  @Post('/signin')
  async signIn(@CurrentUser() user) {
    const auth = await this.authSvc.signin({
      id: user.id,
      username: user.username,
      email: user.email,
      role: user.role,
    });
    return {
      data: user,
      meta: auth,
    };
  }
}
