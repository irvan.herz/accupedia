import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import * as dayjs from 'dayjs';
import { User } from 'src/users/entities/user.entity';
import { Repository } from 'typeorm';
import { SignupWithEmailDto } from './dto/auth.dto';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User)
    private readonly usersRepo: Repository<User>,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(usernameOrEmail: string, password: string) {
    const user = await this.usersRepo.findOneBy([
      { username: usernameOrEmail },
      { email: usernameOrEmail },
    ]);
    if (!user) return null;
    const match = await bcrypt.compare(password, user.password);
    if (!match) return null;
    return user;
  }

  async signin(user: any) {
    const token = this.jwtService.sign({ ...user }, { expiresIn: '5m' });
    const refreshToken = this.jwtService.sign({ ...user }, { expiresIn: '5d' });
    const expiredAt = dayjs().add(30, 'minutes').toDate();
    return {
      token,
      refreshToken,
      expiredAt,
    };
  }

  async signup(payload: SignupWithEmailDto) {
    const result = await this.usersRepo.save(payload);
    return result;
  }

  async validateRefreshToken(refreshToken: string) {
    try {
      const user = await this.jwtService.verifyAsync(refreshToken);
      return user;
    } catch (err) {
      console.log(err);
      return false;
    }
  }
}
