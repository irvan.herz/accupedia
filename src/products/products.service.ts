import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ILike, Repository } from 'typeorm';
import { ProductFilterDto } from './dto/products.dto';
import { Product } from './entities/product.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private readonly productsRepo: Repository<Product>,
  ) {}

  async createWithDto(payload: Partial<Product>) {
    const result = await this.productsRepo.save(payload);
    return result;
  }

  async create(payload: Partial<Product>) {
    const result = await this.productsRepo.save(payload);
    return result;
  }

  async save(payload: Partial<Product>) {
    const result = await this.productsRepo.save(payload);
    return result;
  }

  async findByQuery(filter: ProductFilterDto) {
    const take = filter.limit || 10;
    const page = filter.page || 1;
    const skip = (page - 1) * take;
    const result = this.productsRepo.findAndCount({
      where: {
        title: filter?.search ? ILike(`%${filter.search}%`) : undefined,
      },
      skip,
      take,
      order: { [filter.sortBy]: filter.sortOrder },
    });
    return result;
  }

  async updateById(id: number, payload: Partial<Product>) {
    const result = await this.productsRepo.update({ id }, payload);
    return result.affected;
  }

  async findById(id: number) {
    const result = await this.productsRepo.findOneBy({ id });
    return result;
  }

  async findBySku(sku: string) {
    const result = await this.productsRepo.findOneBy({ sku });
    return result;
  }

  async deleteById(id: number) {
    const result = await this.productsRepo.delete(id);
    return result.affected;
  }
}
