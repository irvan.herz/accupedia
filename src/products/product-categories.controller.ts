import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import {
  CreateProductCategoryDto,
  ProductCategoryFilterDto,
  UpdateProductCategoryDto,
} from './dto/products.dto';
import { ProductCategoriesService } from './product-categories.service';

@Controller('products/categories')
export class ProductCategoriesController {
  constructor(private readonly categoriesSvc: ProductCategoriesService) {}

  @Post()
  async create(@Body() payload: CreateProductCategoryDto) {
    await this.categoriesSvc.create(payload as any);
    return;
  }

  @Get()
  async findByQuery(@Query() filter: ProductCategoryFilterDto) {
    const [data, numItems] = await this.categoriesSvc.findByQuery(filter);
    const numPages = Math.ceil(numItems / filter.limit);
    const meta = {
      page: filter.page,
      limit: filter.limit,
      numItems,
      numPages,
    };
    return { data, meta };
  }

  @Get(':id')
  async findById(@Param('id') id: number) {
    const data = await this.categoriesSvc.findById(+id);
    return { data };
  }

  @Patch(':id')
  async updateById(
    @Param('id') id: number,
    @Body() payload: UpdateProductCategoryDto,
  ) {
    await this.categoriesSvc.updateById(+id, payload as any);
    return;
  }

  @Delete(':id')
  async deleteById(@Param('id') id: number) {
    await this.categoriesSvc.deleteById(+id);
    return;
  }
}
