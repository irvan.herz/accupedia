import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ProductCategory } from './product-category.entity';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;
  @Column('varchar', { length: 300 })
  sku: string;
  @Column('varchar', { length: 300 })
  title: string;
  @Column('enum', { enum: ['draft', 'published'], default: 'draft' })
  status: 'draft' | 'published';
  @Column('int', { default: null, nullable: true })
  categoryId: number;
  @Column('int', { default: 0 })
  quantity: string;
  @Column('decimal', { default: 0 })
  price: string;
  @Column('json', { default: {} })
  images: any;
  @Column('text')
  description: string;
  @CreateDateColumn()
  createdAt: Date;
  @UpdateDateColumn()
  updatedAt: Date;

  @ManyToOne(() => ProductCategory, {
    cascade: true,
    nullable: true,
    onDelete: 'SET NULL',
    eager: true,
  })
  category: ProductCategory;
}
