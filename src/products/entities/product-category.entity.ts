import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class ProductCategory {
  @PrimaryGeneratedColumn()
  id: number;
  @Column('varchar', { length: 300 })
  name: string;
  @Column('text')
  description: string;
  @Column('enum', { enum: ['draft', 'published'], default: 'draft' })
  status: 'draft' | 'published';
  @CreateDateColumn()
  createdAt: Date;
  @UpdateDateColumn()
  updatedAt: Date;
}
