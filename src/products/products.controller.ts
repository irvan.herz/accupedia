import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import {
  CreateProductDto,
  ProductFilterDto,
  UpdateProductDto,
} from './dto/products.dto';
import { ProductsService } from './products.service';

@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @Post()
  async create(@Body() payload: CreateProductDto) {
    await this.productsService.create(payload as any);
    return;
  }

  @Get()
  async findByQuery(@Query() filter: ProductFilterDto) {
    const [data, numItems] = await this.productsService.findByQuery(filter);
    const numPages = Math.ceil(numItems / filter.limit);
    const meta = {
      page: filter.page,
      limit: filter.limit,
      numItems,
      numPages,
    };
    return { data, meta };
  }

  @Get(':id')
  async findById(@Param('id') id: number) {
    const data = await this.productsService.findById(+id);
    return { data };
  }

  @Patch(':id')
  async updateById(@Param('id') id: number, @Body() payload: UpdateProductDto) {
    await this.productsService.updateById(+id, payload as any);
    return;
  }

  @Delete(':id')
  async deleteById(@Param('id') id: number) {
    await this.productsService.deleteById(+id);
    return;
  }
}
