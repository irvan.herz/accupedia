import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ILike, Repository } from 'typeorm';
import { ProductCategoryFilterDto } from './dto/products.dto';
import { ProductCategory } from './entities/product-category.entity';

@Injectable()
export class ProductCategoriesService {
  constructor(
    @InjectRepository(ProductCategory)
    private readonly categoriesRepo: Repository<ProductCategory>,
  ) {}

  async create(payload: Partial<ProductCategory>) {
    const result = await this.categoriesRepo.save(payload);
    return result;
  }

  async save(payload: Partial<ProductCategory>) {
    const result = await this.categoriesRepo.save(payload);
    return result;
  }

  async findByQuery(filter: ProductCategoryFilterDto) {
    const take = filter.limit || 10;
    const page = filter.page || 1;
    const skip = (page - 1) * take;
    const result = this.categoriesRepo.findAndCount({
      where: {
        name: filter?.search ? ILike(`%${filter.search}%`) : undefined,
      },
      skip,
      take,
      order: { [filter.sortBy]: filter.sortOrder },
    });
    return result;
  }

  async updateById(id: number, payload: Partial<ProductCategory>) {
    const result = await this.categoriesRepo.update({ id }, payload);
    return result.affected;
  }

  async findById(id: number) {
    const result = await this.categoriesRepo.findOneBy({ id });
    return result;
  }

  async deleteById(id: number) {
    const result = await this.categoriesRepo.delete(id);
    return result.affected;
  }
}
