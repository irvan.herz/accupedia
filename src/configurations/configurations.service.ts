import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Configuration } from './entities/configuration.entity';

@Injectable()
export class ConfigurationsService {
  constructor(
    @InjectRepository(Configuration)
    private readonly configRepo: Repository<Configuration>,
  ) {}

  async create(payload: Partial<Configuration>) {
    const result = await this.configRepo.save(payload);
    return result;
  }

  async save(payload: Partial<Configuration>) {
    const result = await this.configRepo.save(payload);
    return result;
  }

  async findMany() {
    const result = await this.configRepo.findAndCount();
    return result;
  }

  async findById(id: number) {
    const result = await this.configRepo.findOneBy({ id });
    return result;
  }

  async findByName(name: string) {
    const result = await this.configRepo.findOneBy({ name });
    return result;
  }

  async updateByName(name: string, payload: Partial<Configuration>) {
    const result = await this.configRepo.update({ name }, payload);
    return result.affected;
  }

  async deleteByName(name: string) {
    const result = await this.configRepo.delete({ name });
    return result.affected;
  }
}
