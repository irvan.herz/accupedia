import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { CurrentUser } from 'src/shared/current-user.decorator';
import { JwtAuthGuard } from 'src/shared/jwt-auth.guard';
import { UpdateUserDto, UserFilterDto } from './dto/users.dto';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(private readonly usersSvc: UsersService) {}

  @Post('/setup')
  async setup() {
    return this.usersSvc.create({
      email: 'admin@admin.com',
      username: 'admin',
      fullName: 'Admin',
      role: 'admin',
      password: bcrypt.hashSync('admin', 5),
    });
  }

  @Get()
  async findByQuery(@Query() filter: UserFilterDto) {
    const [data, numItems] = await this.usersSvc.findByQuery(filter);
    const numPages = Math.ceil(numItems / filter.limit);
    const meta = {
      page: filter.page,
      limit: filter.limit,
      numItems,
      numPages,
    };
    return { data, meta };
  }

  @Get(':id')
  async findById(@Param('id') id: number) {
    const data = await this.usersSvc.findById(+id);
    return { data };
  }

  @UseGuards(JwtAuthGuard)
  @Get('username/:username')
  async findByUsername(
    @Param('username') username: string,
    @CurrentUser() currentUser: CurrentUser,
  ) {
    if (username === 'me') username = currentUser.username;
    const data = await this.usersSvc.findByUsername(username);
    return { data };
  }

  @Patch(':id')
  async updateById(@Param('id') id: number, @Body() payload: UpdateUserDto) {
    await this.usersSvc.updateById(+id, payload as any);
    return;
  }

  @Delete(':id')
  async deleteById(@Param('id') id: number) {
    await this.usersSvc.deleteById(+id);
    return;
  }
}
