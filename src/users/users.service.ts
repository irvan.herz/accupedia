import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DataSource, ILike, Repository } from 'typeorm';
import { UserFilterDto } from './dto/users.dto';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly usersRepo: Repository<User>,
    private readonly dataSource: DataSource,
  ) {}

  async create(payload: Partial<User>) {
    const result = await this.usersRepo.save(payload);
    return result;
  }

  async save(payload: Partial<User>) {
    const result = await this.usersRepo.save(payload);
    return result;
  }

  async findByQuery(filter: UserFilterDto) {
    const take = filter.limit || 10;
    const page = filter.page || 1;
    const skip = (page - 1) * take;
    const result = this.usersRepo.findAndCount({
      where: {
        fullName: filter?.search ? ILike(`%${filter.search}%`) : undefined,
      },
      skip,
      take,
      order: { [filter.sortBy]: filter.sortUser },
    });
    return result;
  }

  async updateById(id: number, payload: Partial<User>) {
    const result = await this.usersRepo.update({ id }, payload);
    return result.affected;
  }

  async findById(id: number) {
    const result = await this.usersRepo.findOneBy({ id });
    return result;
  }

  async findByUsername(username: string) {
    const result = await this.usersRepo.findOneBy({ username });
    return result;
  }

  async findByEmail(email: string) {
    const result = await this.usersRepo.findOneBy({ email });
    return result;
  }

  async deleteById(id: number) {
    const result = await this.usersRepo.delete(id);
    return result.affected;
  }
}
