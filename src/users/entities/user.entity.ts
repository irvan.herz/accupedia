import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

type RoleType = 'user' | 'admin';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;
  @Column('varchar', { length: 300, unique: true })
  username: string;
  @Column('varchar', { length: 300, unique: true })
  email: string;
  @Column('varchar', { length: 300, nullable: true })
  password: string;
  @Column({ type: 'enum', enum: ['user', 'admin'], default: 'user' })
  role: RoleType;
  @Column('varchar', { length: 300, nullable: true })
  fullName: string;
  @CreateDateColumn()
  createdAt: Date;
  @UpdateDateColumn()
  updatedAt: Date;
}
